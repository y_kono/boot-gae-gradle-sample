package jp.co.sekainet.sample.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@Entity @Data @NoArgsConstructor @AllArgsConstructor @Builder(toBuilder = true)
public class User {

    @Id
    private Long id;

    private String name;
}
