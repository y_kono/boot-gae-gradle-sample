package jp.co.sekainet.sample;

import com.googlecode.objectify.ObjectifyFilter;
import com.googlecode.objectify.ObjectifyService;
import jp.co.sekainet.sample.entity.User;
import lombok.val;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * ObjectifyConfig.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@Configuration
public class ObjectifyConfig {

    @Bean
    FilterRegistrationBean ofyFilter() {
        val register = new FilterRegistrationBean(new ObjectifyFilter());
        register.setOrder(2);
        register.addUrlPatterns("/*");
        return register;
    }

    @Bean
    ServletListenerRegistrationBean<ServletContextListener> ofyListener() {
        return new ServletListenerRegistrationBean<>(new ServletContextListener() {
            @Override
            public void contextInitialized(ServletContextEvent sce) {
                ObjectifyService.register(User.class);
            }

            @Override
            public void contextDestroyed(ServletContextEvent sce) {}
        });
    }
}
