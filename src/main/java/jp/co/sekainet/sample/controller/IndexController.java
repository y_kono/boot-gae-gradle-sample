package jp.co.sekainet.sample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * IndexController.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@Controller @RequestMapping("")
public class IndexController {

    @GetMapping
    public String index() {
        return "index.html";
    }
}
