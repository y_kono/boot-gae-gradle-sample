package jp.co.sekainet.sample.resource;

import com.google.appengine.api.modules.ModulesException;
import com.google.appengine.api.modules.ModulesServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.googlecode.objectify.ObjectifyService;
import jp.co.sekainet.sample.entity.User;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * SessionTestResource.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@RestController @RequestMapping("test")
@SessionScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionTestResource implements Serializable {

    private static int seed;
    private int sessionId;

    @GetMapping @SneakyThrows
    public Map<String, Object> test() {
        Random random = SecureRandom.getInstanceStrong();
        if (seed == 0) seed = random.nextInt();
        if (sessionId == 0) sessionId = random.nextInt();

        val resp = new HashMap<String, Object>();
        resp.put("applicationId", SystemProperty.applicationId.get());
        resp.put("applicationVersion", SystemProperty.applicationVersion.get());
        resp.put("environment", SystemProperty.environment.get());
        resp.put("version", SystemProperty.version.get());

        try {
            val modulesService = ModulesServiceFactory.getModulesService();
            resp.put("getCurrentInstanceId", modulesService.getCurrentInstanceId());
            resp.put("getCurrentModule", modulesService.getCurrentModule());
            resp.put("getCurrentVersion", modulesService.getCurrentVersion());
            resp.put("getModules", modulesService.getModules());
        } catch (ModulesException e) {}

        resp.put("_seed", seed);
        resp.put("_sessionId", sessionId);
        resp.put("日本語", "こんにちは");

        return resp;
    }


    @RequestMapping("ofy")
    public String ofySample() {
        val user = User.builder().name("John Due").build();
        ObjectifyService.ofy().save().entity(user).now();

        val users = ObjectifyService.ofy().load().type(User.class).list();

        val resultMap = new HashMap<String, Object>();
        resultMap.put("java version", System.getProperty("java.version"));
        resultMap.put("add user", user);
        resultMap.put("list user", users);
        return resultMap.toString();
    }

}
