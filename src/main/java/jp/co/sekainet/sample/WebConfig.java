package jp.co.sekainet.sample;

import lombok.val;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.*;
import java.io.IOException;

/**
 * WebConfig.
 *
 * @author Yasutaka KOUNO<y.kono@sekainet.co.jp>
 */
@Configuration
public class WebConfig {

    @Bean
    public FilterRegistrationBean characterEncodingFilter() {
        val register = new FilterRegistrationBean(new Filter() {
            @Override
            public void init(FilterConfig filterConfig) throws ServletException {}

            @Override
            public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
                    throws IOException, ServletException {
                request.setCharacterEncoding("UTF-8");
                response.setCharacterEncoding("UTF-8");
                chain.doFilter(request, response);
            }

            @Override
            public void destroy() {}
        });
        register.setOrder(1);
        register.addUrlPatterns("/*");
        return register;
    }
}
