# Google App Engine for Java8 (Standard) & Spring Boot & Objectify Sample
GAE/J8 Standard環境でのSpring bootとObjectifyの使用設定サンプル

## App Engineの環境準備
*事前にプロジェクトを作成してプロジェクトIDを控えておくこと。*

以下のコマンドを実行。オプションはよしなに対応してください。
* gcloud init
* gcloud auth application-default login
* gcloud components install app-engine-java
* gcloud components update
* gcloud app create

## コマンド
開発時に使用する主なコマンド。
* `./gradlew appRun` ローカル環境での実行.
* `./gradlew appDeploy` App Engine環境へのデプロイ.
* `gcloud app browse` Appをブラウザに表示